package jp.alhinc.fuse_yuri.calculate_sales;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

public class CalculateSales {
	public static void main(String[] args) {
		HashMap<String, String> map = new HashMap<String, String>();
		HashMap<String, Long> map2 = new HashMap<String, Long>();

		BufferedReader br = null;

		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		try {
			//支店定義ファイルの入力
			File file = new File(args[0],"branch.lst");

            if (!file.exists()) {
                System.out.println("支店定義ファイルが存在しません");
                return;
            }

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			//支店コードと支店名を分割
			String line;
			while ((line = br.readLine()) != null) {
				String[] keys = line.split(",");
				if(keys.length != 2 || !keys[0].matches("^[0-9]{3}$")) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
				//支店コードと支店名を分割
				map.put(keys[0], keys[1]);
				map2.put(keys[0],(long)0);
			}
		}catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}finally {
			if(br != null)
				try {
					br.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
		}


		try {
			//指定したパスに存在するファイル・ディレクトリを取得
			//listFilesメソッドを使用して一覧を取得
			File dir = new File(args[0]);
			File[] lists = dir.listFiles();

			ArrayList <File> salesList = new ArrayList<File>();
			for(int i=0; i< lists.length; i++) {
				if(lists[i].getName().matches("^[0-9]{8}.rcd$") && lists[i].isFile()) {
					salesList.add(lists[i]);
				}
			}

			//連番 歯抜けになっている場合
			for(int i = 0; i<salesList.size()-1;i++) {
				long n= Long.parseLong(salesList.get(i).getName().substring(0,8));
				long n2= Long.parseLong(salesList.get(i+1).getName().substring(0,8));

				if(!(n == n2-1)) {
					System.out.println("売上ファイル名が連番になっていません");
					return;
				}
			}

			for(int i=0; i< salesList.size(); i++) {
				FileReader fr2 = new FileReader(salesList.get(i));
				br = new BufferedReader(fr2);

				String data;
				ArrayList <String> dataList = new ArrayList <String>();
				while((data = br.readLine()) != null) {
					dataList.add(data);
				}

				//売上ファイルの行数が1行以下・3行以上の場合
				int dataListDig = dataList.size();
				String fileName = salesList.get(i).getName();
				int fileDig =fileName.length();

				if(dataListDig !=2) {
					System.out.println(fileName.substring(fileDig-12)+"のフォーマットが不正です");
					return;
				}

				//売上ファイルの支店コードが支店定義ファイルに存在しない場合
				if(!map.containsKey(dataList.get(0))) {
					System.out.println(fileName.substring(fileDig-12)+"の支店コードが不正です");
					return;
				}

				//売上金額 に数字以外が含まれている場合
				if(!dataList.get(1).matches("^[0-9]+$")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

				Long sum = map2.get(dataList.get(0)) + Long.parseLong(dataList.get(1));

				//支店別集計合計金額が10桁超えた
				int sumDigit = String.valueOf(sum).length();
				if(sumDigit >10) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				map2.put(dataList.get(0),sum);
			}
		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}finally {
			if(br != null)
				try {
					br.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
		}

		//出力部分
		if (!printOut(args[0],"branch.out",map,map2)) {
			return;
		}
		
	}
	
	
	//メソッド部分
	public static boolean printOut(String path,String fileName,HashMap<String,String> m,HashMap<String,Long> m2) {
		
		PrintWriter pw = null;
		try {
			File fileOut = new File(path,fileName);
            FileWriter fw = new FileWriter(fileOut);
            BufferedWriter bw = new BufferedWriter(fw);
            pw = new PrintWriter(bw);
            	 
            for (Object code:m.keySet()) {
                pw.println(code+","+m.get(code)+","+m2.get(code));
            }
            
		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally {
			if(pw != null)
				pw.close();
		}
		return true;
	}
	
}
